/*
 * Copyright (c) 2013-2015, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "evmcast.h"
#include "mcast.h"
#include <arpa/inet.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

struct address
{
  char* addr;
  int   port;
};

struct evmcast_config
{
  int            nodes_count[MAX_N_OF_GROUPS];
  int            groups_count;
  struct address nodes[MAX_N_OF_GROUPS][MAX_N_OF_NODES];
};

enum option_type
{
  option_boolean,
  option_integer,
  option_string,
  option_verbosity,
  option_bytes
};

struct option
{
  const char*      name;
  void*            value;
  enum option_type type;
};

struct option mcast_options[] = { { "verbosity", &mcast_config.verbosity, option_verbosity },
                                  { "tcp-nodelay", &mcast_config.tcp_nodelay, option_boolean },
                                  { "message-timeout", &mcast_config.message_timeout, option_integer },
                                  { "batch-size", &mcast_config.batch_size, option_integer },
                                  { "batch-max-interval", &mcast_config.batch_max_interval, option_integer },
                                  { "avoid-convoy", &mcast_config.avoid_convoy, option_boolean },
                                  { "replica-mode", &mcast_config.replica_mode, option_boolean },
                                  { "paxos", &mcast_config.paxos, option_string },
                                  /* lmdb storage configuration */
                                  { "persist", &mcast_config.persist, option_boolean },
                                  { "trash-files", &mcast_config.trash_files, option_boolean },
                                  { "lmdb-sync", &mcast_config.lmdb_sync, option_boolean },
                                  { "lmdb-env-path", &mcast_config.lmdb_env_path, option_string },
                                  { "lmdb-mapsize", &mcast_config.lmdb_mapsize, option_bytes },
                                  { 0 } };

static int parse_line(struct evmcast_config* c, char* line);

static void address_init(struct address* a, char* addr, int port);

static void address_free(struct address* a);

static struct sockaddr_in address_to_sockaddr(struct address* a);

struct evmcast_config*
evmcast_config_read(const char* path)
{
  struct stat            sb;
  FILE*                  f = NULL;
  char                   line[512];
  int                    linenumber = 1;
  struct evmcast_config* c = NULL;

  if ((f = fopen(path, "r")) == NULL) {
    perror("fopen");
    goto failure;
  }

  if (stat(path, &sb) == -1) {
    perror("stat");
    goto failure;
  }

  if (!S_ISREG(sb.st_mode)) {
    mcast_log_error("Error: %s is not a regular file\n", path);
    goto failure;
  }

  c = malloc(sizeof(struct evmcast_config));
  if (c == NULL) {
    perror("malloc");
    goto failure;
  }
  memset(c, 0, sizeof(struct evmcast_config));

  while (fgets(line, sizeof(line), f) != NULL) {
    if (line[0] != '#' && line[0] != '\n') {
      if (parse_line(c, line) == 0) {
        mcast_log_error("Please, check line %d\n", linenumber);
        mcast_log_error("Error parsing config file %s\n", path);
        goto failure;
      }
    }
    linenumber++;
  }

  fclose(f);
  return c;

failure:
  free(c);
  if (f != NULL)
    fclose(f);
  return NULL;
}

void
evmcast_config_free(struct evmcast_config* config)
{
  int i, j;

  for (i = 0; i < config->groups_count; ++i)
    for (j = 0; j < config->nodes_count[i]; ++j)
      address_free(&config->nodes[i][j]);
  free(config);
}

struct sockaddr_in
evmcast_node_address(struct evmcast_config* config, int group_id, int node_id)
{
  return address_to_sockaddr(&config->nodes[group_id][node_id]);
}

void
evmcast_node_get_ids(struct evmcast_config* config, struct sockaddr_in addr, int* group_id, int* id)
{
  int i, j;

  for (i = 0; i < config->groups_count; ++i) {
    for (j = 0; j < config->nodes_count[i]; ++j) {
      if ((addr.sin_port = htons(config->nodes[i][j].port)) &&
          (addr.sin_addr.s_addr = inet_addr(config->nodes[i][j].addr))) {
        *group_id = i;
        *id = j;
      }
    }
  }
  *group_id = *id = -1;
}

int
evmcast_node_listen_port(struct evmcast_config* config, int group_id, int node_id)
{
  return config->nodes[group_id][node_id].port;
}

int
evmcast_node_count(struct evmcast_config* config, int group_id)
{
  return config->nodes_count[group_id];
}

int
evmcast_group_count(struct evmcast_config* config)
{
  return config->groups_count;
}

static char*
strtrim(char* string)
{
  char *s, *t;
  for (s = string; isspace(*s); s++)
    ;
  if (*s == 0)
    return s;
  t = s + strlen(s) - 1;
  while (t > s && isspace(*t))
    t--;
  *++t = '\0';
  return s;
}

static int
parse_bytes(char* str, size_t* bytes)
{
  char* end;
  errno = 0; /* To distinguish strtoll's return value 0 */
  *bytes = strtoull(str, &end, 10);
  if (errno != 0)
    return 0;
  while (isspace(*end))
    end++;
  if (*end != '\0') {
    if (strcasecmp(end, "kb") == 0)
      *bytes *= 1024;
    else if (strcasecmp(end, "mb") == 0)
      *bytes *= 1024 * 1024;
    else if (strcasecmp(end, "gb") == 0)
      *bytes *= 1024 * 1024 * 1024;
    else
      return 0;
  }
  return 1;
}

static int
parse_boolean(char* str, int* boolean)
{
  if (str == NULL)
    return 0;
  if (strcasecmp(str, "yes") == 0) {
    *boolean = 1;
    return 1;
  }
  if (strcasecmp(str, "no") == 0) {
    *boolean = 0;
    return 1;
  }
  return 0;
}

static int
parse_integer(char* str, int* integer)
{
  int   n;
  char* end;
  if (str == NULL)
    return 0;
  n = strtol(str, &end, 10);
  if (end == str)
    return 0;
  *integer = n;
  return 1;
}

static int
parse_string(char* str, char** string)
{
  if (str == NULL || str[0] == '\0' || str[0] == '\n')
    return 0;
  *string = strdup(str);
  return 1;
}

static int
parse_address(char* str, struct evmcast_config* c)
{
  int  port, group_id, node_id;
  char address[128];
  int  rv = sscanf(str, "%d %d %s %d", &group_id, &node_id, address, &port);
  if (rv == 4 && group_id < MAX_N_OF_GROUPS && node_id < MAX_N_OF_NODES) {
    c->nodes_count[group_id]++;
    if (group_id > (c->groups_count - 1))
      c->groups_count = group_id + 1;
    address_init(&(c->nodes[group_id][node_id]), address, port);
    return 1;
  }
  return 0;
}

static int
parse_verbosity(char* str, mcast_log_level* verbosity)
{
  if (strcasecmp(str, "quiet") == 0)
    *verbosity = MCAST_LOG_QUIET;
  else if (strcasecmp(str, "error") == 0)
    *verbosity = MCAST_LOG_ERROR;
  else if (strcasecmp(str, "info") == 0)
    *verbosity = MCAST_LOG_INFO;
  else if (strcasecmp(str, "debug") == 0)
    *verbosity = MCAST_LOG_DEBUG;
  else
    return 0;
  return 1;
}

static struct option*
lookup_option(char* opt)
{
  int i = 0;
  while (mcast_options[i].name != NULL) {
    if (strcasecmp(mcast_options[i].name, opt) == 0)
      return &mcast_options[i];
    i++;
  }
  return NULL;
}

static int
parse_line(struct evmcast_config* c, char* line)
{
  int            rv = 0;
  char*          tok;
  char*          sep = " ";
  struct option* opt;

  line = strtrim(line);
  tok = strsep(&line, sep);

  if (strcasecmp(tok, "n") == 0 || strcasecmp(tok, "node") == 0) {
    if (!parse_address(line, c)) {
      mcast_log_error("Number of groups/nodes_per_group exceeded maximum of: %d/%d\n", MAX_N_OF_GROUPS, MAX_N_OF_NODES);
      return 0;
    }
    return 1;
  }

  line = strtrim(line);
  opt = lookup_option(tok);
  if (opt == NULL)
    return 0;

  switch (opt->type) {
    case option_boolean:
      rv = parse_boolean(line, opt->value);
      if (rv == 0)
        mcast_log_error("Expected 'yes' or 'no'\n");
      break;
    case option_integer:
      rv = parse_integer(line, opt->value);
      if (rv == 0)
        mcast_log_error("Expected number\n");
      break;
    case option_string:
      rv = parse_string(line, opt->value);
      if (rv == 0)
        mcast_log_error("Expected string\n");
      break;
    case option_verbosity:
      rv = parse_verbosity(line, opt->value);
      if (rv == 0)
        mcast_log_error("Expected quiet, error, info, or debug\n");
      break;
    case option_bytes:
      rv = parse_bytes(line, opt->value);
      if (rv == 0)
        mcast_log_error("Expected number of bytes.\n");
  }

  return rv;
}

static void
address_init(struct address* a, char* addr, int port)
{
  a->addr = strdup(addr);
  a->port = port;
}

static void
address_free(struct address* a)
{
  free(a->addr);
}

static struct sockaddr_in
address_to_sockaddr(struct address* a)
{
  struct sockaddr_in addr;
  memset(&addr, 0, sizeof(struct sockaddr_in));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(a->port);
  addr.sin_addr.s_addr = inet_addr(a->addr);
  return addr;
}
