/*
 * Copyright (c) 2015-2017, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcast_types_pack.h"

#define MSGPACK_OBJECT_AT(obj, i) (obj->via.array.ptr[i].via)

static void
msgpack_pack_string(msgpack_packer* p, char* buffer, int len)
{
#if MSGPACK_VERSION_MAJOR > 0
  msgpack_pack_bin(p, len);
  msgpack_pack_bin_body(p, buffer, len);
#else
  msgpack_pack_raw(p, len);
  msgpack_pack_raw_body(p, buffer, len);
#endif
}

static void
msgpack_unpack_uint32_at(msgpack_object* o, uint32_t* v, int* i)
{
  *v = (uint32_t)MSGPACK_OBJECT_AT(o, *i).u64;
  (*i)++;
}

static void
msgpack_unpack_uint16_at(msgpack_object* o, uint16_t* v, int* i)
{
  *v = (uint16_t)MSGPACK_OBJECT_AT(o, *i).u64;
  (*i)++;
}

static void
msgpack_unpack_string_at(msgpack_object* o, char** buffer, int* len, int* i)
{
  *buffer = NULL;
#if MSGPACK_VERSION_MAJOR > 0
  *len = MSGPACK_OBJECT_AT(o, *i).bin.size;
  const char* obj = MSGPACK_OBJECT_AT(o, *i).bin.ptr;
#else
  *len = MSGPACK_OBJECT_AT(o, *i).raw.size;
  const char* obj = MSGPACK_OBJECT_AT(o, *i).raw.ptr;
#endif
  if (*len > 0) {
    *buffer = malloc(*len);
    memcpy(*buffer, obj, *len);
  }
  (*i)++;
}

static void
msgpack_pack_mcast_value(msgpack_packer* p, mcast_value* v)
{
  msgpack_pack_string(p, v->mcast_value_val, v->mcast_value_len);
}

static void
msgpack_unpack_mcast_value_at(msgpack_object* o, mcast_value* v, int* i)
{
  msgpack_unpack_string_at(o, &v->mcast_value_val, &v->mcast_value_len, i);
}

void
msgpack_pack_mcast_message(msgpack_packer* p, mcast_message* v)
{
  int i;
  msgpack_pack_array(p, (7 + v->to_groups_len));
  msgpack_pack_int32(p, v->type);
  msgpack_pack_uint64(p, v->uid);
  msgpack_pack_uint16(p, v->from_group);
  msgpack_pack_uint16(p, v->from_node);
  msgpack_pack_uint32(p, v->timestamp);
  msgpack_pack_uint16(p, v->to_groups_len);
  for (i = 0; i < v->to_groups_len; i++) {
    msgpack_pack_uint16(p, v->to_groups[i]);
  }
  msgpack_pack_mcast_value(p, &v->value);
}

void
msgpack_unpack_mcast_message(msgpack_object* o, mcast_message* v)
{
  int i = 2, j;
  v->type = MSGPACK_OBJECT_AT(o, 0).u64;
  v->uid = MSGPACK_OBJECT_AT(o, 1).u64;
  msgpack_unpack_uint16_at(o, &v->from_group, &i);
  msgpack_unpack_uint16_at(o, &v->from_node, &i);
  msgpack_unpack_uint32_at(o, &v->timestamp, &i);
  msgpack_unpack_uint16_at(o, &v->to_groups_len, &i);
  for (j = 0; j < v->to_groups_len; j++)
    msgpack_unpack_uint16_at(o, &v->to_groups[j], &i);
  msgpack_unpack_mcast_value_at(o, &v->value, &i);
}
