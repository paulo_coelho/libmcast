/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "evmcast.h"
#include "message_mcast.h"
#include <arpa/inet.h>
#include <errno.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/thread.h>
#include <inttypes.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct mcast_replica
{
  m_uid_t                  uid;
  int                      group_count;
  struct bufferevent*      bev;
  mcast_replica_connect_cb ccb;
  mcast_replica_reply_cb   rcb;
  void*                    cb_arg;
};

static void on_replica_connect(struct bufferevent* bev, short events, void* arg);
static void on_replica_read(struct bufferevent* bev, void* arg);
static void socket_set_nodelay(int fd);

struct mcast_replica*
mcast_replica_connect(int group_id, int id, const char* config, struct event_base* base, mcast_replica_connect_cb ccb,
                      mcast_replica_reply_cb rcb, void* cb_arg)
{
  struct sockaddr_in     addr;
  struct evmcast_config* conf = evmcast_config_read(config);
  struct mcast_replica*  c = NULL;
  static int             first_time = 1;
  struct timespec        nanos;

  if (first_time) {
    clock_gettime(CLOCK_MONOTONIC, &nanos);
    srandom(nanos.tv_nsec);
    first_time = 0;
  }

  if (conf == NULL) {
    mcast_log_error("Failed to read configuration file %s\n", config);
    return NULL;
  }

  if (!mcast_config.replica_mode) {
    mcast_log_error("Replica mode is disabled in configuration file %s\n", config);
    return NULL;
  }
  evthread_use_pthreads();

  c = malloc(sizeof(struct mcast_replica));
  c->bev = bufferevent_socket_new(base, -1, BEV_OPT_CLOSE_ON_FREE | BEV_OPT_THREADSAFE);
  c->uid = random() & 0xffffffff;
  c->ccb = ccb;
  c->rcb = rcb;
  c->cb_arg = cb_arg;
  c->group_count = evmcast_group_count(conf);
  addr = evmcast_node_address(conf, group_id, id);
  bufferevent_setcb(c->bev, on_replica_read, NULL, on_replica_connect, c);
  evbuffer_enable_locking(bufferevent_get_input(c->bev), NULL);
  evbuffer_enable_locking(bufferevent_get_output(c->bev), NULL);
  bufferevent_enable(c->bev, EV_READ | EV_WRITE);
  bufferevent_socket_connect(c->bev, (struct sockaddr*)&addr, sizeof(addr));
  socket_set_nodelay(bufferevent_getfd(c->bev));
  evmcast_config_free(conf);
  mcast_register_replica(c->bev, group_id, c->uid);
  return c;
}

void
mcast_replica_submit(struct mcast_replica* c, mcast_message* m)
{
  m->type = MCAST_CLIENT;
  send_mcast_message(c->bev, m);
}

void
mcast_replica_free(struct mcast_replica* c)
{
  bufferevent_free(c->bev);
  free(c);
}

m_uid_t
mcast_replica_get_uid(struct mcast_replica* c)
{
  return c->uid;
}

int
mcast_replica_get_group_count(struct mcast_replica* c)
{
  return c->group_count;
}

/* STATIC FUNCTIONS */
static void
socket_set_nodelay(int fd)
{
  int flag = mcast_config.tcp_nodelay;
  setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &flag, sizeof(int));
}

static void
on_replica_read(struct bufferevent* bev, void* arg)
{
  struct mcast_replica* c = arg;
  mcast_message         msg;
  struct evbuffer*      in = bufferevent_get_input(bev);

  while (recv_mcast_message(in, &msg)) {
    c->rcb(c, &msg, c->cb_arg);
    mcast_message_content_free(&msg);
  }
}

static void
on_replica_connect(struct bufferevent* bev, short events, void* arg)
{
  struct mcast_replica* c = arg;

  if (events & BEV_EVENT_CONNECTED) {
    mcast_log_info("Replica %d: connected to node\n", c->uid);
    if (c->ccb)
      c->ccb(c, c->cb_arg);
  } else {
    mcast_log_error("Replica %d: socket ERROR: %s\n", c->uid, evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR()));
  }
}
