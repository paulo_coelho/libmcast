/*
 * Copyright (c) 2015, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mcast.h"
#include "mcast_node.h"
#include "message_mcast.h"
#include "peers_mcast.h"
#include <event2/event.h>
#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

struct evmcast_node
{
  int                    id;
  int                    group_id;
  m_ts_t                 timestamp;
  mcast_deliver_function delfun;
  void*                  delarg;
  struct mcast_node*     state;
  struct mcast_peers*    mcast_peers;
  // TODO: batch messages from client using parameters mcast_config.batch_size and mcast_config.batch_max_interval
};

static void mcast_peer_send_mcast_message(struct mcast_peer* p, void* arg);
static void evmcast_node_generic_handle(struct mcast_peer* p, mcast_message* msg, void* arg);
static void evmcast_node_handle_client(struct mcast_peer* p, mcast_message* msg, void* arg);
static void evmcast_node_handle_replica(struct mcast_peer* p, mcast_message* msg, void* arg);

struct evmcast_node*
evmcast_node_init_internal(int group_id, int id, struct evmcast_config* c, struct mcast_peers* mcast_peers,
                           mcast_deliver_function cb, void* arg)
{
  int                  port = evmcast_node_listen_port(c, group_id, id);
  int                  rv = mcast_peers_listen(mcast_peers, port);
  struct evmcast_node* n;

  if (rv == 0) {
    mcast_log_error("Could not start listening on port %d", port);
    return NULL;
  }

  mcast_peers_connect_to_nodes(mcast_peers);
  n = malloc(sizeof(struct evmcast_node));
  n->id = id;
  n->group_id = group_id;
  n->timestamp = 0;
  n->delfun = cb;
  n->delarg = arg;
  n->state = mcast_node_new(n->id, n->group_id);
  n->mcast_peers = mcast_peers;
  mcast_peers_subscribe(mcast_peers, MCAST_CLIENT, evmcast_node_handle_client, n);
  mcast_peers_subscribe(mcast_peers, MCAST_REPLICA, evmcast_node_handle_replica, n);
  mcast_peers_subscribe(mcast_peers, MCAST_START, evmcast_node_generic_handle, n);
  return n;
}

struct evmcast_node*
evmcast_node_init(int group_id, int id, const char* config_file, struct event_base* base, mcast_deliver_function cb,
                  void* arg)
{
  struct mcast_peers*    mcast_peers = NULL;
  struct evmcast_node*   n = NULL;
  struct evmcast_config* config = evmcast_config_read(config_file);

  if (config == NULL)
    return NULL;

  // Check id validity of node and group
  if (id < 0 || id >= MAX_N_OF_NODES || group_id < 0 || group_id > MAX_N_OF_GROUPS) {
    mcast_log_error("Invalid mcast_node group_id/node_id: %d/%d", group_id, id);
    evmcast_config_free(config);
    return NULL;
  }

  mcast_peers = mcast_peers_new(base, config);
  n = evmcast_node_init_internal(group_id, id, config, mcast_peers, cb, arg);
  if (n == NULL)
    mcast_peers_free(mcast_peers);

  evmcast_config_free(config);
  return n;
}

void
evmcast_node_free_internal(struct evmcast_node* n)
{
  mcast_node_free(n->state);
  free(n);
}

void
evmcast_node_free(struct evmcast_node* n)
{
  mcast_peers_free(n->mcast_peers);
  evmcast_node_free_internal(n);
}

void
evmcast_node_submit(struct evmcast_node* n, mcast_message* m, int skip_own_group)
{
  uint16_t* dst;
  int       i, dst_len;

  mcast_log_debug("Multicasting msg(type, uid, gid, nid) = (%s, %" PRIu64 ", %d, %d)...",
                  mcast_message_get_type_as_string(m->type), m->uid, m->from_group, m->from_node);
  dst = m->to_groups;
  dst_len = m->to_groups_len;
  for (i = 0; i < dst_len; i++) {
    if (skip_own_group && dst[i] == n->group_id)
      continue;

    mcast_peers_foreach_node_in_group(n->mcast_peers, dst[i], mcast_peer_send_mcast_message, m);
  }
}

struct mcast_node*
evmcast_node_get_state(struct evmcast_node* n)
{
  return n->state;
}

void
evmcast_node_deliver(struct evmcast_node* n, mcast_message* m)
{
  int                unset_sender = 1;
  struct mcast_peer* p;

  if (!mcast_node_is_delivered(n->state, m->uid)) {
    if (!m->timestamp)
      m->timestamp = ++n->timestamp;

    if (mcast_config.replica_mode)
      mcast_peers_foreach_replica(n->mcast_peers, mcast_peer_send_mcast_message, m);

    p = mcast_peers_get_sender(n->mcast_peers, m->uid, unset_sender);
    if (p)
      n->delfun(mcast_peer_get_buffer(p), m, n->delarg);
    else
      n->delfun(NULL, m, n->delarg);
    
    mcast_node_deliver(n->state, m);
    mcast_log_debug("Delivering msg(type, uid, ts, gid, nid) = (%s, %" PRIu64 ", %u, %d, %d)...",
                    mcast_message_get_type_as_string(m->type), m->uid, m->timestamp, m->from_group, m->from_node);
  }
}

/* STATIC FUNCTIONS */
static void
mcast_peer_send_mcast_message(struct mcast_peer* p, void* arg)
{
  send_mcast_message(mcast_peer_get_buffer(p), arg);
}

static void
evmcast_node_generic_handle(struct mcast_peer* p, mcast_message* msg, void* arg)
{
  struct evmcast_node* n = arg;
  evmcast_node_deliver(n, msg);
}

static void
evmcast_node_handle_client(struct mcast_peer* p, mcast_message* msg, void* arg)
{
  struct evmcast_node* n = arg;
  msg->from_group = n->group_id;
  msg->from_node = n->id;
  msg->timestamp = 0;
  mcast_node_set_uid(n->state, msg);
  msg->type = MCAST_START;
  mcast_peers_set_sender(n->mcast_peers, msg->uid, p);
  evmcast_node_submit(n, msg, 0);

  /* ATOMIC MULTICAST USES THE MULTICAST STATE DIFFERENTLY, NOT SAVING HERE!
  if (!mcast_node_multicast(n->state, msg))
    mcast_log_error("Unable to persist message %" PRIu64, msg->uid);
  */
  msg->type = MCAST_CLIENT;
}

static void
evmcast_node_handle_replica(struct mcast_peer* p, mcast_message* msg, void* arg)
{
  struct evmcast_node* n = arg;
  int                  group = msg->from_group;
  int                  id = msg->from_node;

  mcast_peer_set_as_replica(p, group, id);
  mcast_log_info("Node %d / %d: Replica %d / %d registered successfully", n->group_id, n->id, group, id);
}
