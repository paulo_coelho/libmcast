# LibMCast

This is an implementation of multicast algorithms.

LibMCast is divided in two parts: reliable multicast and genuine atomic multicast. 

The folder ```mcast``` implements the core of the multicast mechanism and
related structures, and is not cluttered with network specific code.

In ```evmcast``` and ```evamcast``` we have the actual networked
multicast implementation for reliable multicast and atomic multicast, respectively.  
They are built with [msgpack][2] for serialization and [libevent][1] for communication.
Consensus is implemented by [libpaxos][3].

There are 2 versions of atomic multicast available: fault-tolerant version of Skeen's algorithm, which we call BaseCast; and [FastCast][7], which can deliver multipartition messages in four communication steps.

## Building

These are the basic steps required to get and compile LibPaxos3

	git clone https://bitbucket.org/paulo_coelho/libmcast.git
	mkdir libmcast/build
	cd libmcast/build
	cmake ..
	make

LibMCast depends on [libevent][1] ,[msgpack][2] and [libpaxos][3]. By default,
LibMCast uses an in-memory storage.  You can store in disk if you have [lmdb][4] installed.
(see ```sample``` for configuration details).

LibMCast should compile on Linux and OS X.

### Useful build options

You pass options to cmake as follows: ```cmake -DOPTION=VALUE```

- ```LIBEVENT_ROOT=PATH``` -  point it to your installation of Libevent
- ```MSGPACK_ROOT=PATH``` - point it to your installation of MessagePack
- ```LIBPAXOS_ROOT=PATH``` - point it to your installation of LibPaxos3
- ```LMDB_ROOT=PATH``` - point it to your installation of LMDB (if you want to persistent storage)

## Running the examples

Copy conf and script files to folder where the samples were compiled and run:

	cd libmcast/buid/sample
	cp ../../sample/*conf .
	cp ../../sample/*sh .
	./run-2g3p.sh amcast|mcast|famcast

## Configuration

See ```sample/*.conf``` for sample configuration files.


## License

The library is under development. Use at your own risk :) 

All rights reserved to [USI][6].

[1]: http://www.libevent.org
[2]: http://www.msgpack.org
[3]: https://bitbucket.org/sciascid/libpaxos
[4]: https://github.com/LMDB/lmdb
[5]: https://github.com/google/googletest
[6]: http://inf.usi.ch
[7]: http://www.inf.usi.ch/faculty/pedone/Paper/2017/2017DSN.pdf
