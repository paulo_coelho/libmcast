/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Implementation of the baseline algorithm,
 * providing atomic multicast in 6 communication steps (best case)
 */

#include "carray.h"
#include "evngamcast.h"
#include "mcast.h"
#include "mcast_node.h"
#include "message_mcast.h"
#include "peers_mcast.h"
#include <assert.h>
#include <errno.h>
#include <event2/event.h>
#include <evpaxos.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <string.h>

#define TRIM_INTERVAL 10 // seconds

struct evngamcast_node
{
  int                    id;
  int                    group_id;
  int                    leader_id;         // id of the group leader
  mcast_deliver_function delfun;            // Delivery callback
  void*                  delarg;            // The argument to the delivery callback
  struct mcast_node*     state;             // keeps state of received messages
  struct mcast_peers*    mcast_peers;       // Manages all connections
  struct timeval         tv;                // TODO: trimming, leader election and recovery
  struct event*          timeout_ev;        // TODO: trimming, leader election and recovery
  unsigned int           k_p, k_d;          // last proposed and decided instances, respectively
  int                    current_prop_size; // current size of proposal set (to compare with batch-size property)
  struct evlearner*      learner;           // paxos proposer, acceptor and learner
  struct bufferevent*    prop_bev;          // buffer to send proposes
  const char*            pconfig;           // paxos config file
  unsigned               next_paxos_trim;   // TODO: next trim to be sent to paxos (periodically)
  struct carray*         pend;              // array of pending msgs
  struct timeval         last_prop;         // last proposal timestamp
  struct timeval         proposal_tv;       // timeout between proposals
  struct event*          proposal_ev;       // proposal event
};

/* algorithm tasks */
static void timed_events(evutil_socket_t fd, short event, void* arg);

static void when_decided(unsigned iid, char* value, size_t size, void* arg);

static void try_to_adeliver(struct evngamcast_node* n);

static void on_connect(struct bufferevent* bev, short events, void* arg);

static void
mcast_peer_send_mcast_message(struct mcast_peer* p, void* arg)
{
  send_mcast_message(mcast_peer_get_buffer(p), arg);
}

static struct bufferevent*
evngamcast_node_connect_to_proposer(struct evngamcast_node* n)
{
  int                 flag = 1;
  struct bufferevent* bev;
  struct event_base*  base = mcast_peers_get_event_base(n->mcast_peers);
  mcast_log_info("Connecting to proposer...");
  struct evpaxos_config* conf = evpaxos_config_read(n->pconfig);
  if (conf == NULL) {
    mcast_log_error("Failed to read config file %s\n", n->pconfig);
    return NULL;
  }
  struct sockaddr_in addr = evpaxos_proposer_address(conf, n->leader_id);
  bev = bufferevent_socket_new(base, -1, BEV_OPT_CLOSE_ON_FREE);
  bufferevent_setcb(bev, NULL, NULL, on_connect, n);
  bufferevent_enable(bev, EV_READ | EV_WRITE);
  bufferevent_socket_connect(bev, (struct sockaddr*)&addr, sizeof(addr));
  setsockopt(bufferevent_getfd(bev), IPPROTO_TCP, TCP_NODELAY, &flag, sizeof(int));
  evpaxos_config_free(conf);
  return bev;
}

static void
on_connect(struct bufferevent* bev, short events, void* arg)
{
  struct evngamcast_node* n = arg;
  if (events & BEV_EVENT_CONNECTED) {
    mcast_log_info("...Connected to proposer");
  } else {
    mcast_log_error("%s\n", evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR()));
    if (n->prop_bev != NULL)
      bufferevent_free(n->prop_bev);
    evngamcast_node_connect_to_proposer(n);
  }
}

static void
evngamcast_node_submit_to_proposer(struct evngamcast_node* n, char* value, int size)
{
  if (n->prop_bev == NULL) {
    n->prop_bev = evngamcast_node_connect_to_proposer(n);
  }
  paxos_submit(n->prop_bev, value, size);
}

static void
evngamcast_node_send_mcast_message(struct evngamcast_node* n, mcast_message* m)
{
  uint16_t* dst;
  int       i, dst_len;

  dst = m->to_groups;
  dst_len = m->to_groups_len;
  for (i = 0; i < dst_len; i++) {
    mcast_peers_foreach_node_in_group(n->mcast_peers, dst[i], mcast_peer_send_mcast_message, m);
  }
}

static void
evngamcast_node_send_to_replicas(struct evngamcast_node* n, mcast_message* m)
{
  mcast_peers_foreach_replica(n->mcast_peers, mcast_peer_send_mcast_message, m);
}

static void
handle_replica(struct mcast_peer* p, mcast_message* msg, void* arg)
{
  struct evngamcast_node* node = arg;
  int                     group = msg->from_group;
  int                     id = msg->from_node;
  mcast_peer_set_as_replica(p, group, id);
  mcast_log_info("Node %d / %d: Replica %d / %d registered successfully", node->group_id, node->id, group, id);
}

static void
handle_client(struct mcast_peer* p, mcast_message* msg, void* arg)
{
  struct evngamcast_node* node = arg;

  msg->from_group = node->group_id;
  msg->from_node = node->id;
  msg->timestamp = 0;
  mcast_node_set_uid(node->state, msg);
  msg->type = MCAST_START;
  mcast_peers_set_sender(node->mcast_peers, msg->uid, p);
  evngamcast_node_send_mcast_message(node, msg);
  msg->type = MCAST_CLIENT;
}

static void
handle_start(struct mcast_peer* p, mcast_message* msg, void* arg)
{
  struct evngamcast_node* node = arg;
  mcast_message*          clone;

  mcast_node_multicast(node->state, msg);                                  // save the message in multicast
  clone = mcast_message_clone(msg, 0);                                     // we don't need the data anymore!
  if (clone->from_group == node->group_id && clone->from_node == node->id) // if mine, I propose
    evngamcast_node_submit_to_proposer(node, (char*)clone, sizeof(mcast_message));
  mcast_log_debug("START: (uid, gid, nid) = (%llu, %d, %d)", clone->uid, clone->from_group, clone->from_node);
}

static void
check_timeouts(evutil_socket_t fd, short event, void* arg)
{
  struct evngamcast_node* n = arg;
  static unsigned         trim_instance = 1;

  if (n->id == 0 && n->group_id == 0 && n->k_d > trim_instance) {
    mcast_log_info("Node %d / %d: sending trim for instance %u", n->group_id, n->id, trim_instance);
    evlearner_send_trim(n->learner, trim_instance);
  }

  trim_instance = n->k_d;
  event_add(n->timeout_ev, &n->tv);
}

struct evngamcast_node*
evngamcast_node_init_internal(int group_id, int id, struct evmcast_config* c, struct mcast_peers* mcast_peers,
                              mcast_deliver_function cb, void* arg)
{
  struct evngamcast_node* n;

  n = malloc(sizeof(struct evngamcast_node));
  memset(n, 0, sizeof(struct evngamcast_node));
  n->group_id = group_id;
  n->id = id;
  n->delfun = cb;
  n->delarg = arg;
  n->state = mcast_node_new(n->id, n->group_id);
  n->mcast_peers = mcast_peers;
  n->pend = carray_new(100);

  mcast_peers_subscribe(mcast_peers, MCAST_CLIENT, handle_client, n);
  mcast_peers_subscribe(mcast_peers, MCAST_REPLICA, handle_replica, n);
  mcast_peers_subscribe(mcast_peers, MCAST_START, handle_start, n);

  struct event_base* base = mcast_peers_get_event_base(mcast_peers);

  // general task timeout (trimming, print structures size, ...)
  n->tv.tv_sec = TRIM_INTERVAL;
  n->tv.tv_usec = 0;
  n->timeout_ev = evtimer_new(base, check_timeouts, n);
  event_add(n->timeout_ev, &n->tv);

  // Setup proposal timeout
  n->proposal_tv.tv_sec = mcast_config.message_timeout / 1000000;
  n->proposal_tv.tv_usec = mcast_config.message_timeout % 1000000;
  n->proposal_ev = evtimer_new(base, timed_events, n);
  event_add(n->proposal_ev, &n->proposal_tv);
  mcast_log_info("Node %d started. Proposing interval = %dus", n->id, n->proposal_tv.tv_usec);
  return n;
}

struct evngamcast_node*
evngamcast_node_init(int group_id, int id, const char* mcast_config_file, struct event_base* base,
                     const char* paxos_config_file, mcast_deliver_function cb, void* arg)
{
  deliver_function       f = when_decided;
  struct evmcast_config* mconfig = evmcast_config_read(mcast_config_file);

  if (mconfig == NULL) {
    mcast_log_error("Error loading config file");
    return NULL;
  }

  // Check id validity of node and group
  if (id < 0 || id >= MAX_N_OF_NODES || group_id < 0 || group_id > MAX_N_OF_GROUPS) {
    mcast_log_error("Invalid mcast_node group_id/node_id: %d/%d", group_id, id);
    return NULL;
  }

  // Connect to nodes and start listening
  struct mcast_peers* mcast_peers = mcast_peers_new(base, mconfig);
  mcast_peers_connect_to_nodes(mcast_peers);
  int port = evmcast_node_listen_port(mconfig, group_id, id);
  int rv = mcast_peers_listen(mcast_peers, port);
  if (rv == 0) {
    mcast_log_error("Could not start listening on port %d", port);
    return NULL;
  }
  struct evngamcast_node* n = evngamcast_node_init_internal(group_id, id, mconfig, mcast_peers, cb, arg);

  // paxos initialization
  struct evlearner* learner = evlearner_init(paxos_config_file, f, n, base);
  n->learner = learner;
  n->prop_bev = NULL;
  n->pconfig = paxos_config_file;

  if (learner == NULL) {
    mcast_log_error("Error initializing learner");
    return NULL;
  }
  evmcast_config_free(mconfig);
  return n;
}

void
evngamcast_node_free_internal(struct evngamcast_node* n)
{
  event_free(n->timeout_ev);
  mcast_node_free(n->state);
  event_free(n->proposal_ev);
  carray_free(n->pend);
  free(n);
}

void
evngamcast_node_free(struct evngamcast_node* n)
{
  if (n->learner != NULL)
    evlearner_free(n->learner);
  if (n->prop_bev != NULL)
    bufferevent_free(n->prop_bev);
  mcast_peers_free(n->mcast_peers);
  evngamcast_node_free_internal(n);
}

static long
timeval_diff(struct timeval* t1)
{
  long           us;
  struct timeval t2;
  gettimeofday(&t2, NULL);
  us = (t2.tv_sec - t1->tv_sec) * 1e6;
  if (us < 0)
    return 0;
  us += (t2.tv_usec - t1->tv_usec);
  return us;
}

/****************************
 * ATOMIC MULTICAST RELATED *
 ****************************/

static void
timed_events(evutil_socket_t fd, short event, void* arg)
{
  struct evngamcast_node* n = arg;
  if (timeval_diff(&(n->last_prop)) > mcast_config.message_timeout) {
    try_to_adeliver(n);
  }
  event_add(n->proposal_ev, &n->proposal_tv);
}

static void
when_decided(unsigned k_d, char* value, size_t size, void* arg)
{
  struct evngamcast_node* n = arg;
  mcast_message*          msgs = (mcast_message*)value;
  mcast_message*          m;
  int                     msg_count = size / sizeof(mcast_message), i, j, to_me;

  assert(size % sizeof(mcast_message) == 0);
  n->k_d = k_d;
  for (i = 0; i < msg_count; i++) {
    m = &msgs[i];
    m->timestamp = k_d;
    to_me = 0;
    for (j = 0; j < m->to_groups_len; j++) {
      if (m->to_groups[j] == n->group_id) {
        to_me = 1;
        break;
      }
    }
    if (to_me) {
      mcast_log_debug("DECIDED[%u]: DELIVERING m(type, uid, group) = (%s, %llu, %u)", k_d,
                      mcast_message_get_type_as_string(m->type), m->uid, m->from_group);
      m_uid_t* uid = malloc(sizeof(m_uid_t));
      *uid = m->uid;
      carray_push_back(n->pend, uid);
      try_to_adeliver(n);
    } else {
      mcast_log_debug("DECIDED[%u]: IGNORING m(type, uid, group) = (%s, %llu, %u)", k_d,
                      mcast_message_get_type_as_string(m->type), m->uid, m->from_group);
    }
  }
}

static void
try_to_adeliver(struct evngamcast_node* n)
{
  m_uid_t*           uid;
  mcast_message      start;
  struct mcast_peer* p = NULL;

  while (carray_count(n->pend)) {
    p = NULL;
    uid = NULL;
    uid = carray_check_front(n->pend);

    if (mcast_node_get_multicast(n->state, *uid, &start) == 0) {
      mcast_log_debug("A-DELIVERY: CAN'T DELIVER YET m(uid) = (%llu)", *uid);
      break;
    }

    if (start.from_group == n->group_id && start.from_node == n->id) { // if I proposed, I deliver
      p = mcast_peers_get_sender(n->mcast_peers, start.uid, 1);
      assert(p != NULL);
      mcast_log_debug("A-DELIVERY (REPLYING TO CLIENT): m(uid, ts) = (%llu, %u)", start.uid, start.timestamp);
    }

    // send to replicas (if in replica_mode)
    if (mcast_config.replica_mode) {
      evngamcast_node_send_to_replicas(n, &start);
    }

    // deliver to client
    mcast_node_deliver(n->state, &start);
    n->delfun((p != NULL ? mcast_peer_get_buffer(p) : NULL), &start, n->delarg);
    mcast_message_content_free(&start);
    carray_pop_front(n->pend); // remove from queue
    free(uid);
    mcast_log_debug("A-DELIVERY: DELIVERED m(uid, ts) = (%llu, %u)", start.uid, start.timestamp);
  }
  gettimeofday(&(n->last_prop), NULL);
}
