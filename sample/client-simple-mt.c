/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "stats.h"
#include <errno.h>
#include <evmcast.h>
#include <getopt.h>
#include <inttypes.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define VSIZE 20000

struct client_value
{
  struct timespec t;
  short           dest_len;
  size_t          size;
  char            value[VSIZE];
};

struct client
{
  int                  id;
  struct client_value  v;
  struct mcast_client* c;
  struct event_base*   base;
  int                  stopped;
};

// client configuration parameters - default values
static int         node_id = 0;
static int         group_id = 0;
static int         global = 0;
static int         verbose = 0;
static int         outstanding = 1;
static int         threads = 1;
static int         msg_size = 64;
static const char* config = "mcast.conf";
static int         dest_len = 2;

// server may crash if clients disconnect abruptly
static int     stop = 0;
struct event*  stop_ev = 0;
struct timeval stop_tv = { 2, 0 };

static void
loop_exit(evutil_socket_t fd, short event, void* arg)
{
  struct client* c = arg;
  char           fname[50];

  if (!c->stopped) {
    event_base_loopexit(c->base, NULL);
    sprintf(fname, "client.%d.%d.%d-%d.%d.txt", outstanding, threads, global, dest_len, rand());
    stats_persist(fname);
    c->stopped = 1;
  }
}

static void
handle_sigint(int sig, short ev, void* arg)
{
  struct event_base* base = arg;
  printf("Caught signal %d, waiting for additional messages...\n", sig);
  stop = 1;
  event_base_loopexit(base, NULL);
}

static void
random_string(char* s, const int len)
{
  int               i;
  static const char alphanum[] = "0123456789abcdefghijklmnopqrstuvwxyz";
  for (i = 0; i < len - 1; ++i)
    s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
  s[len - 1] = 0;
}

static void
client_submit_value(struct client* c)
{
  m_gid_t dst[MAX_N_OF_GROUPS];
  int     i;

  if (stop) {
    stop_ev = evtimer_new(c->base, loop_exit, c);
    event_add(stop_ev, &stop_tv);
    return;
  }

  c->v.dest_len = 1;
  dst[0] = group_id;
  clock_gettime(CLOCK_MONOTONIC, &c->v.t);
  if (rand() % 100 < global) {
    c->v.dest_len = dest_len;
    for (i = 1; i < dest_len; ++i)
      dst[i] = (group_id + i) % mcast_client_get_group_count(c->c);
  }

  if (verbose)
    printf("Client sending message '%.16s' to %d group(s)\n", c->v.value, c->v.dest_len);

  mcast_client_submit(c->c, (char*)&c->v, sizeof(struct client_value) - VSIZE + c->v.size, dst, c->v.dest_len);
}

static void
on_connect(struct mcast_client* mc, void* arg)
{
  int            i;
  struct client* c = arg;

  stats_init();
  random_string(c->v.value, msg_size);
  c->v.size = msg_size;
  for (i = 0; i < outstanding; ++i)
    client_submit_value(c);
}

static void
on_read(struct mcast_client* mc, char* resp, int resp_len, void* ctx)
{
  struct client*       c = ctx;
  struct client_value* v = (struct client_value*)resp;
  struct timespec      now;
  long                 latency;

  if (resp_len) {
    clock_gettime(CLOCK_MONOTONIC, &now);
    latency = (now.tv_sec - v->t.tv_sec) * 1e6 + (now.tv_nsec - v->t.tv_nsec) * 1e-3;
    stats_add(latency, v->dest_len);

    if (verbose)
      printf("Client %" PRIu64 " received %zu bytes - value %.16s\n", mcast_client_get_uid(mc), v->size, v->value);

    client_submit_value(c);
  }
}

void*
make_client(void* arg)
{
  struct client* c = arg;

  c->base = event_base_new();
  c->c = mcast_client_connect(group_id, node_id, config, c->base, on_connect, on_read, c);
  if (c->c == NULL) {
    printf("Error connecting, exiting...\n");
  } else {
    event_base_dispatch(c->base);
    printf("Client thread %d: saving stats and exiting.\n", c->id);
  }
  return NULL;
}

static void
client_free(struct client* c)
{
  mcast_client_free(c->c);
  event_base_free(c->base);
  free(c);
}

static void
usage(const char* prog)
{
  printf("Usage: %s -n <node-id> -g <group_id> -c <path/to/mcast.conf> [-a] <percentage> [-s] <msg-size] [-h] [-v]\n",
         prog);
  printf("  %-30s%s\n", "-n, --node-id", "the node id");
  printf("  %-30s%s\n", "-g, --group-id", "the group id");
  printf("  %-30s%s\n", "-c, --config-file", "path to multicast configuration file");
  printf("  %-30s%s\n", "-a, --multigroup-percentage", "porcentage of multigroup commands (defaults to 0)");
  printf("  %-30s%s\n", "-d, --destinations", "number of destination groups for multigroup messages (defaults to 2)");
  printf("  %-30s%s%d%s\n", "-s, --message-size", "size of the message in bytes (defaults to ", msg_size, " bytes)");
  printf("  %-30s%s\n", "-h, --help", "output this message and exit");
  printf("  %-30s%s\n", "-v, --verbose", "increase verbosity (print delivered messages)");
  exit(EXIT_FAILURE);
}

int
main(int argc, char* argv[])
{
  int                  opt = 0, idx = 0;
  struct client**      clients;
  pthread_t*           t;
  struct event_base*   base;
  struct event*        sig;
  static struct option options[] = { { "node-id", required_argument, 0, 'n' },
                                     { "group-id", required_argument, 0, 'g' },
                                     { "config-file", required_argument, 0, 'c' },
                                     { "multigroup-percentage", required_argument, 0, 'a' },
                                     { "destinations", required_argument, 0, 'd' },
                                     { "message-size", required_argument, 0, 's' },
                                     { "outstanding", no_argument, 0, 'o' },
                                     { "threads", no_argument, 0, 'o' },
                                     { "verbose", no_argument, 0, 't' },
                                     { "help", no_argument, 0, 'h' },
                                     { 0, 0, 0, 0 } };

  while ((opt = getopt_long(argc, argv, "hvn:g:c:a:d:s:o:t:", options, &idx)) != -1) {
    switch (opt) {
      case 'n':
        node_id = atoi(optarg);
        break;
      case 'g':
        group_id = atoi(optarg);
        break;
      case 'a':
        global = atoi(optarg);
        break;
      case 'd':
        dest_len = atoi(optarg);
        break;
      case 's':
        msg_size = atoi(optarg);
        break;
      case 'o':
        outstanding = atoi(optarg);
        break;
      case 't':
        threads = atoi(optarg);
        break;
      case 'c':
        config = optarg;
        break;
      case 'v':
        verbose = 1;
        break;
      default:
        usage(argv[0]);
    }
  }

  if (group_id == -1 || node_id == -1 || strlen(config) == 0)
    usage(argv[0]);

  clients = (struct client**)malloc(threads * sizeof(struct client*));
  t = (pthread_t*)malloc(threads * sizeof(pthread_t));
  for (int i = 0; i < threads; ++i) {
    clients[i] = (struct client*)malloc(sizeof(struct client));
    memset(clients[i], 0, sizeof(struct client));
    clients[i]->id = i;
    pthread_create(&t[i], NULL, make_client, clients[i]);
  }
  srand(time(NULL));
  base = event_base_new();
  sig = evsignal_new(base, SIGINT, handle_sigint, base); // clients);
  evsignal_add(sig, NULL);
  event_base_dispatch(base);

  sleep(2);
  for (int i = 0; i < threads; ++i) {
    pthread_join(t[i], NULL);
    client_free(clients[i]);
  }
  printf("Main thread: exiting.\n");
  free(t);
  free(clients);
  event_base_free(base);
  return 0;
}
