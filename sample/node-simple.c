/* * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <evamcast.h>
#include <evfamcast.h>
#include <evmcast.h>
#include <getopt.h>
#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STATS_INTERVAL 5

enum server_type_t
{
  MCAST,
  AMCAST,
  FAMCAST
};

struct stats_ctrl
{
  int            value_size;
  long           delivered;
  struct event*  stats_ev;
  struct timeval stats_interval;
  struct timeval last_tv;
};

static int verbose = 0;
static int noreply = 0;

static void
handle_sigint(int sig, short ev, void* arg)
{
  struct event_base* base = arg;
  printf("Caught signal %d, exiting...\n", sig);
  event_base_loopexit(base, NULL);
}

static void
on_stats(evutil_socket_t fd, short event, void* arg)
{
  struct stats_ctrl* sc = arg;
  int                delta_t = STATS_INTERVAL;

  fprintf(stderr, "%ld msgs/sec, message size %d\n", sc->delivered / delta_t, sc->value_size);
  sc->delivered = 0;
  event_add(sc->stats_ev, &sc->stats_interval);
}

static void
deliver(struct bufferevent* client, mcast_message* m, void* arg)
{
  struct stats_ctrl* sc = arg;
  struct mcast_value val = m->value;

  /* HERE THE NODE IS SUPPOSED TO DO SOMETHING WITH THE RECEIVED MSG,
   * PREPARE A RESPONSE AND REPLY BACK.
   *
   * THIS SIMPLE EXAMPLE ONLY ECHOES THE RECEIVED MESSAGE BACK  */
  if (client && !noreply)
    send_mcast_message(client, m);

  if (verbose)
    printf("Delivering message with (uid, ts) = (%" PRIu64 ", %u) and %d bytes\n", m->uid, m->timestamp,
           val.mcast_value_len);

  sc->value_size = val.mcast_value_len;
  ++sc->delivered;
}

static void
dispatch(struct event_base* base, struct stats_ctrl* sc)
{
  struct event* sig;

  // statistics
  memset(sc, 0, sizeof(struct stats_ctrl));
  sc->stats_interval = (struct timeval){ STATS_INTERVAL, 0 };
  sc->stats_ev = evtimer_new(base, on_stats, sc);
  gettimeofday(&(sc->last_tv), NULL);
  event_add(sc->stats_ev, &sc->stats_interval);

  // signal handling
  sig = evsignal_new(base, SIGINT, handle_sigint, base);
  evsignal_add(sig, NULL);
  signal(SIGPIPE, SIG_IGN);
  event_base_dispatch(base);
  event_free(sc->stats_ev);
  event_free(sig);
}

static void
start_node_mcast(int group_id, int id, const char* config)
{
  struct event_base*     base;
  struct evmcast_node*   n;
  mcast_deliver_function cb = deliver;
  struct stats_ctrl      sc;

  base = event_base_new();
  n = evmcast_node_init(group_id, id, config, base, cb, &sc);
  if (n == NULL) {
    printf("Could not start the node.\n");
  } else {
    dispatch(base, &sc);
    evmcast_node_free(n);
  }

  event_base_free(base);
}

static void
start_node_amcast(int group_id, int id, const char* config, const char* paxos_config)
{
  struct event_base*     base;
  struct evamcast_node*  n;
  struct stats_ctrl      sc;
  mcast_deliver_function cb = deliver;

  base = event_base_new();
  n = evamcast_node_init(group_id, id, config, paxos_config, base, cb, &sc, NULL);
  if (n == NULL) {
    printf("Could not start the node.\n");
  } else {
    dispatch(base, &sc);
    evamcast_node_free(n);
  }

  event_base_free(base);
}

static void
start_node_famcast(int group_id, int id, const char* config, const char* paxos_config)
{
  struct event_base*     base;
  struct evfamcast_node* n;
  struct stats_ctrl      sc;
  mcast_deliver_function cb = deliver;

  base = event_base_new();
  n = evfamcast_node_init(group_id, id, config, paxos_config, base, cb, &sc);
  if (n == NULL) {
    printf("Could not start the node.\n");
  } else {
    dispatch(base, &sc);
    evfamcast_node_free(n);
  }

  event_base_free(base);
}

static void
usage(const char* prog)
{
  printf("Usage: %s -n <node-id> -g <group_id> -c <path/to/mcast.conf> -s <server_type> [-p <path/to/paxos.conf>] [-h] "
         "[-s]\n",
         prog);
  printf("  %-30s%s\n", "-n, --node-id", "the node id");
  printf("  %-30s%s\n", "-g, --group-id", "the group id");
  printf("  %-30s%s\n", "-c, --config-file", "path to multicast configuration file");
  printf("  %-30s%s\n", "-s, --server-type", "server type to be started: mcast (default), amcast or famcast");
  printf("  %-30s%s\n", "-p, ---paxos-file", "path to paxos configuration file");
  printf("  %-30s%s\n", "-0, --no-reply", "send no reply back to client");
  printf("  %-30s%s\n", "-h, --help", "output this message and exit");
  printf("  %-30s%s\n", "-v, --verbose", "increase verbosity (print delivered messages)");
  exit(EXIT_FAILURE);
}

int
main(int argc, char* argv[])
{
  int                group_id = -1, id = -1;
  int                opt = 0, idx = 0;
  const char*        config = "";
  const char*        paxos_config = "";
  enum server_type_t server = MCAST;

  static struct option options[] = { { "node-id", required_argument, 0, 'n' },
                                     { "group-id", required_argument, 0, 'g' },
                                     { "config-file", required_argument, 0, 'c' },
                                     { "server-type", required_argument, 0, 's' },
                                     { "paxos-file", required_argument, 0, 'p' },
                                     { "verbose", no_argument, 0, 'v' },
                                     { "no-reply", no_argument, 0, '0' },
                                     { "help", no_argument, 0, 'h' },
                                     { 0, 0, 0, 0 } };

  while ((opt = getopt_long(argc, argv, "hvn:g:c:s:p:", options, &idx)) != -1) {
    switch (opt) {
      case 'n':
        id = atoi(optarg);
        break;
      case 'g':
        group_id = atoi(optarg);
        break;
      case 'c':
        config = optarg;
        break;
      case 's':
        if (strcasecmp(optarg, "amcast") == 0)
          server = AMCAST;
        else if (strcasecmp(optarg, "famcast") == 0)
          server = FAMCAST;
        else
          server = MCAST;
        break;
      case 'p':
        paxos_config = optarg;
        break;
      case 'v':
        verbose = 1;
        break;
      case '0':
        noreply = 1;
        break;
      default:
        usage(argv[0]);
    }
  }

  if (group_id == -1 || id == -1 || strlen(config) == 0)
    usage(argv[0]);

  if (server != MCAST && strlen(paxos_config) == 0) {
    printf("\nONLY 'MCAST' SERVER TYPE DOES NOT REQUIRE A PAXOS CONFIG FILE!\n\n");
    usage(argv[0]);
  }

  switch (server) {
    case MCAST:
      printf("RUNNING MCAST NODE %d, %d...\n", group_id, id);
      start_node_mcast(group_id, id, config);
      break;
    case AMCAST:
      printf("RUNNING AMCAST NODE %d, %d...\n", group_id, id);
      start_node_amcast(group_id, id, config, paxos_config);
      break;
    case FAMCAST:
      printf("RUNNING FAMCAST NODE %d, %d...\n", group_id, id);
      start_node_famcast(group_id, id, config, paxos_config);
      break;
    default:
      usage(argv[0]);
  }

  return EXIT_SUCCESS;
}
