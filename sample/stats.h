#ifndef __STATS_H__
#define __STATS_H__

#define STATS_MAX_COUNT 10000000

void   stats_init();
void   stats_destroy();
void   stats_add(long latency, short dest_len);
double stats_get_avg();
double stats_get_stddev();
double stats_get_count();
void   stats_print();
void   stats_persist(const char* file);

#endif
