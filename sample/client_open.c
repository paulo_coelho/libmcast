/*
 * Copyright (c) 2013-2015, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "stats.h"
#include <errno.h>
#include <event2/event.h>
#include <evpaxos.h>
#include <netinet/tcp.h>
#include <paxos.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define MAX_VALUE_SIZE 8192

struct client_value
{
	int client_id;
	struct timeval t;
	size_t size;
	char value[ 0 ];
};

struct stats
{
	int delivered_count;
	size_t size;
};

struct client
{
	int id;
	int value_size;
	int reqs_per_sec;
	char* send_buffer;
	struct stats stats;
	struct event_base* base;
	struct bufferevent* bev;
	struct event* stats_ev;
	struct timeval stats_interval;
	struct event* sig;
	struct evlearner* learner;
	struct event* send_ev;
	struct timeval send_start;
	struct timeval send_interval;
	int total_sent;
};


static void
handle_sigint(int sig, short ev, void* arg)
{
	struct client* c = arg;
	struct event_base* base = c->base;
	char file[ 48 ];

	sprintf(file, "stats-%dreqs-%u.txt", c->reqs_per_sec, getpid( ));
	printf("Caught signal %d\n", sig);
	event_base_loopexit(base, NULL);
	stats_print( );
	stats_persist(file);
	stats_destroy( );
}

static void
random_string(char* s, const int len)
{
	int i;
	static const char alphanum[] = "0123456789abcdefghijklmnopqrstuvwxyz";
	for (i = 0; i < len - 1; ++i)
		s[ i ] = alphanum[ rand( ) % (sizeof(alphanum) - 1) ];
	s[ len - 1 ] = 0;
}

static void
client_submit_value(struct client* c)
{
	struct client_value* v = ( struct client_value* ) c->send_buffer;
	gettimeofday(&v->t, NULL);
	size_t size = sizeof(struct client_value) + v->size;
	paxos_submit(c->bev, c->send_buffer, size);
}

// Returns t2 - t1 in microseconds.
static long
timeval_diff(struct timeval* t1, struct timeval* t2)
{
	long us;
	us = (t2->tv_sec - t1->tv_sec) * 1e6;
	if (us < 0)
		return 0;
	us += (t2->tv_usec - t1->tv_usec);
	return us;
}

static void
update_stats(struct stats* stats, struct client_value* delivered, size_t size)
{
	struct timeval tv;
	long lat = 0;

	stats->delivered_count++;
	stats->size = size;
	gettimeofday(&tv, NULL);
	lat = timeval_diff(&delivered->t, &tv);
	stats_add(lat);
}

static void
on_deliver(unsigned iid, char* value, size_t size, void* arg)
{
	struct client* c = arg;
	struct client_value* v = ( struct client_value* ) value;
	if (v->client_id == c->id) {
		update_stats(&c->stats, v, size);
	}
}

static void
on_stats(evutil_socket_t fd, short event, void* arg)
{
	struct client* c = arg;
	float mbps = (c->stats.size * c->stats.delivered_count) * 8 / (1024 * 1024);
	printf("%d value/sec, %.2fMbps\n", c->stats.delivered_count, mbps);
	memset(&c->stats, 0, sizeof(struct stats));
	event_add(c->stats_ev, &c->stats_interval);
}

static void
on_send_requests(int fd, short event, void* arg)
{
	struct client* c = arg;

	struct timeval now;
	gettimeofday(&now, NULL);
	int run_time_usec = timeval_diff(&c->send_start, &now);
	int send_target = c->reqs_per_sec * (( float ) run_time_usec / 1000000.0);
	int to_send = send_target - c->total_sent;
	while (to_send--) {
		c->total_sent++;
		client_submit_value(c);
	}

	evtimer_add(c->send_ev, &c->send_interval);
}

static void
on_connect(struct bufferevent* bev, short events, void* arg)
{
	struct client* c = arg;
	struct client_value* v = ( struct client_value* ) c->send_buffer;
	if (events & BEV_EVENT_CONNECTED) {
		printf("Connected to proposer\n");
		v->size = c->value_size;
		v->client_id = c->id;
		random_string(v->value, v->size);
		stats_init( );
		// schedule event for sending requests
		gettimeofday(&c->send_start, NULL);
		c->send_ev = evtimer_new(c->base, on_send_requests, c);
		evtimer_add(c->send_ev, &c->send_interval);
	} else {
		printf("%s\n", evutil_socket_error_to_string(EVUTIL_SOCKET_ERROR( )));
	}
}

static struct bufferevent*
connect_to_proposer(struct client* c, const char* config, int proposer_id)
{
	struct bufferevent* bev;
	struct evpaxos_config* conf = evpaxos_config_read(config);
	if (conf == NULL) {
		printf("Failed to read config file %s\n", config);
		return NULL;
	}
	struct sockaddr_in addr = evpaxos_proposer_address(conf, proposer_id);
	bev = bufferevent_socket_new(c->base, -1, BEV_OPT_CLOSE_ON_FREE);
	bufferevent_setcb(bev, NULL, NULL, on_connect, c);
	bufferevent_enable(bev, EV_READ | EV_WRITE);
	bufferevent_socket_connect(bev, ( struct sockaddr* ) &addr, sizeof(addr));
	int flag = 1;
	setsockopt(bufferevent_getfd(bev), IPPROTO_TCP, TCP_NODELAY, &flag, sizeof(int));
	return bev;
}

static struct client*
make_client(const char* config, int proposer_id, int reqs_per_sec, int value_size)
{
	struct client* c;
	c = malloc(sizeof(struct client));
	c->base = event_base_new( );

	memset(&c->stats, 0, sizeof(struct stats));
	c->bev = connect_to_proposer(c, config, proposer_id);
	if (c->bev == NULL)
		exit(1);

	c->id = rand( );
	c->value_size = value_size;
	c->reqs_per_sec = reqs_per_sec;
	printf("client with %d reqs/sec\n", c->reqs_per_sec);
	c->send_buffer = malloc(sizeof(struct client_value) + value_size);

	c->stats_interval = (struct timeval) {1, 0};
	c->stats_ev = evtimer_new(c->base, on_stats, c);
	event_add(c->stats_ev, &c->stats_interval);

	c->send_interval = (struct timeval) {0, 1000};// every 1ms

	paxos_config.learner_catch_up = 0;
	c->learner = evlearner_init(config, on_deliver, c, c->base);

	c->sig = evsignal_new(c->base, SIGINT, handle_sigint, c);
	evsignal_add(c->sig, NULL);

	return c;
}

static void
client_free(struct client* c)
{
	free(c->send_buffer);
	bufferevent_free(c->bev);
	event_free(c->stats_ev);
	event_free(c->sig);
	event_free(c->send_ev);
	event_base_free(c->base);
	if (c->learner)
		evlearner_free(c->learner);
	free(c);
}

static void
start_client(const char* config, int proposer_id, int reqs_per_sec, int value_size)
{
	struct client* client;
	client = make_client(config, proposer_id, reqs_per_sec, value_size);
	signal(SIGPIPE, SIG_IGN);
	event_base_dispatch(client->base);
	client_free(client);
}

static void
usage(const char* name)
{
	printf("Usage: %s [path/to/paxos.conf] [-h] [-o] [-v] [-p]\n", name);
	printf("  %-30s%s\n", "-h, --help", "Output this message and exit");
	printf("  %-30s%s\n", "-r, --reqs-per-sec #", "Number of client values to submit per second");
	printf("  %-30s%s\n", "-v, --value-size #", "Size of client value (in bytes)");
	printf("  %-30s%s\n", "-p, --proposer-id #", "d of the proposer to connect to");
	exit(1);
}

int
main(int argc, char const* argv[])
{
	int i = 1;
	int proposer_id = 0;
	int reqs_per_sec = 1;
	int value_size = 64;
	struct timeval seed;
	const char* config = "../paxos.conf";

	if (argc > 1 && argv[ 1 ][ 0 ] != '-') {
		config = argv[ 1 ];
		i++;
	}

	while (i != argc) {
		if (strcmp(argv[ i ], "-h") == 0 || strcmp(argv[ i ], "--help") == 0)
			usage(argv[ 0 ]);
		else if (strcmp(argv[ i ], "-r") == 0 || strcmp(argv[ i ], "--reqs-per-sec") == 0)
			reqs_per_sec = atoi(argv[ ++i ]);
		else if (strcmp(argv[ i ], "-v") == 0 || strcmp(argv[ i ], "--value-size") == 0)
			value_size = atoi(argv[ ++i ]);
		else if (strcmp(argv[ i ], "-p") == 0 || strcmp(argv[ i ], "--proposer-id") == 0)
			proposer_id = atoi(argv[ ++i ]);
		else
			usage(argv[ 0 ]);
		i++;
	}

	gettimeofday(&seed, NULL);
	srand(seed.tv_usec);
	start_client(config, proposer_id, reqs_per_sec, value_size);

	return 0;
}
