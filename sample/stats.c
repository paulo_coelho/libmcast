#include "stats.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

static long*           a; // absolute time
static long*           e; // elapsed time between calls to stats_add
static short*          d; // number of destination groups for message
static unsigned        count;
static struct timespec then;

void
stats_init()
{
  e = malloc(STATS_MAX_COUNT * sizeof(long));
  memset(e, 0, STATS_MAX_COUNT * sizeof(long));
  a = malloc(STATS_MAX_COUNT * sizeof(long));
  memset(a, 0, STATS_MAX_COUNT * sizeof(long));
  d = malloc(STATS_MAX_COUNT * sizeof(short));
  memset(d, 0, STATS_MAX_COUNT * sizeof(short));
  count = 0;
  clock_gettime(CLOCK_MONOTONIC, &then);
}

void
stats_destroy()
{
  count = 0;
  if (a)
    free(a);
  if (e)
    free(e);
}

void
stats_add(long latency, short dest_len)
{
  struct timespec now;

  if (count == STATS_MAX_COUNT) {
    printf("Stats error: array is full");
    return;
  }

  clock_gettime(CLOCK_MONOTONIC, &now);
  a[count] = (now.tv_sec - then.tv_sec) * 1e6 + (now.tv_nsec - then.tv_nsec) * 1e-3;
  e[count] = latency;
  d[count++] = dest_len;
  then = now;
}

double
stats_get_avg()
{
  int    i;
  double avg = 0;

  for (i = 0; i < count; ++i)
    avg += (e[i] - avg) / (i + 1);

  return avg;
}

double
stats_get_stddev()
{
  int    i;
  double var, quad = 0, avg = stats_get_avg();

  for (i = 0; i < count; ++i)
    quad += ((e[i] - avg) * (e[i] - avg));

  var = quad / (count);
  return sqrt(var);
}

double
stats_get_count()
{
  return count;
}

void
stats_print()
{
  printf("Statistics with %d entries average of (%.2f +/- %.2f)us", count, stats_get_avg(), stats_get_stddev());
}

void
stats_persist(const char* file)
{
  int   i;
  long  abs = 0;
  FILE* f = fopen(file, "w");

  if (f) {
    fprintf(f, "#ORDER\tLATENCY\tABS\tDLEN\n");
    for (i = 0; i < count; i++) {
      abs += a[i];
      fprintf(f, "%d\t%ld\t%ld\t%hd\n", (i + 1), e[i], abs, d[i]);
    }

    fclose(f);
  } else {
    printf("Stats: could not save statistics to file '%s'", file);
  }
}
