/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <errno.h>
#include <evmcast.h>
#include <getopt.h>
#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define VSIZE 20000

struct client_value
{
  struct timeval t;
  short          global;
  size_t         size;
  unsigned long  key;
  char           value[VSIZE];
};

// client configuration parameters - default values
static int         node_id = 0;
static int         group_id = 0;
static int         verbose = 0;
static const char* config = "";

static void
handle_sigint(int sig, short ev, void* arg)
{
  struct event_base* base = arg;
  printf("Caught signal %d\n", sig);
  event_base_loopexit(base, NULL);
}

static void
on_connect(struct mcast_replica* mr, void* arg)
{
  printf("Connected...");
}

static void
on_read(struct mcast_replica* mr, mcast_message* msg, void* ctx)
{
  struct client_value* v = (struct client_value*)msg->value.mcast_value_val;
  int                  resp_len = msg->value.mcast_value_len;

  if (resp_len) {
    if (verbose)
      printf("Replica %" PRIu64 " received %zu bytes - key %lu, value %.16s\n", mcast_replica_get_uid(mr), v->size,
             v->key, v->value);
  } else {
    printf("Empty message\n");
  }
}

static void
usage(const char* prog)
{
  printf("Usage: %s -n <node-id> -g <group_id> -c <path/to/mcast.conf> [-a] <percentage> [-s] <msg-size] [-h] [-v]\n",
         prog);
  printf("  %-30s%s\n", "-n, --node-id", "the node id");
  printf("  %-30s%s\n", "-g, --group-id", "the group id");
  printf("  %-30s%s\n", "-c, --config-file", "path to multicast configuration file");
  printf("  %-30s%s\n", "-h, --help", "output this message and exit");
  printf("  %-30s%s\n", "-v, --verbose", "increase verbosity (print delivered messages)");
  exit(EXIT_FAILURE);
}

int
main(int argc, char* argv[])
{
  int                   opt = 0, idx = 0;
  struct mcast_replica* mr;
  struct event_base*    base;
  struct event*         sig;
  static struct option  options[] = { { "node-id", required_argument, 0, 'n' },
                                     { "group-id", required_argument, 0, 'g' },
                                     { "config-file", required_argument, 0, 'c' },
                                     { "verbose", no_argument, 0, 'v' },
                                     { "help", no_argument, 0, 'h' },
                                     { 0, 0, 0, 0 } };

  while ((opt = getopt_long(argc, argv, "hvn:g:c:", options, &idx)) != -1) {
    switch (opt) {
      case 'n':
        node_id = atoi(optarg);
        break;
      case 'g':
        group_id = atoi(optarg);
        break;
      case 'c':
        config = optarg;
        break;
      case 'v':
        verbose = 1;
        break;
      default:
        usage(argv[0]);
    }
  }

  if (group_id == -1 || node_id == -1 || strlen(config) == 0)
    usage(argv[0]);

  srand(time(NULL));
  base = event_base_new();
  mr = mcast_replica_connect(group_id, node_id, config, base, on_connect, on_read, NULL);
  if (mr != NULL) {
    sig = evsignal_new(base, SIGINT, handle_sigint, base);
    evsignal_add(sig, NULL);
    event_base_dispatch(base);
    event_free(sig);
    mcast_replica_free(mr);
  }
  event_base_free(base);
  return 0;
}
