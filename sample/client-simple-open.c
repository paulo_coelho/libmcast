/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "stats.h"
#include <errno.h>
#include <evmcast.h>
#include <getopt.h>
#include <inttypes.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define VSIZE 20000

struct client_value
{
  struct timespec t;
  short           dest_len;
  size_t          size;
  char            value[VSIZE];
};

struct client
{
  struct client_value  v;
  struct mcast_client* c;
  struct event_base*   base;
  struct event*        send_ev;
  struct timeval       send_start;
  struct timeval       send_interval;
  int                  total_sent;
};

// client configuration parameters - default values
static int         node_id = 0;
static int         group_id = 0;
static int         global = 0;
static int         verbose = 0;
static int         reqs_per_sec = 1;
static int         msg_size = 64;
static const char* config = "mcast.conf";
static int         dest_len = 2;

// server may crash if clients disconnect abruptly
static int     stop = 0;
struct event*  stop_ev = 0;
struct timeval stop_tv = { 2, 0 };

static void
loop_exit(evutil_socket_t fd, short event, void* arg)
{
  struct event_base* base = arg;
  char               fname[50];

  printf("...Now exiting.\n");
  event_base_loopexit(base, NULL);
  sprintf(fname, "client.%d.%d-%d.%d.txt", reqs_per_sec, global, dest_len, rand());
  stats_persist(fname);
}

static void
handle_sigint(int sig, short ev, void* arg)
{
  struct event_base* base = arg;
  printf("Caught signal %d, waiting for additional messages...\n", sig);
  stop = 1;
  stop_ev = evtimer_new(base, loop_exit, base);
  event_add(stop_ev, &stop_tv);
}

static void
random_string(char* s, const int len)
{
  int               i;
  static const char alphanum[] = "0123456789abcdefghijklmnopqrstuvwxyz";
  for (i = 0; i < len - 1; ++i)
    s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
  s[len - 1] = 0;
}

static void
client_submit_value(struct client* c)
{
  m_gid_t dst[MAX_N_OF_GROUPS];
  int     i;

  if (stop)
    return;

  c->v.dest_len = 1;
  dst[0] = group_id;
  clock_gettime(CLOCK_MONOTONIC, &c->v.t);
  if (rand() % 100 < global) {
    c->v.dest_len = dest_len;
    for (i = 1; i < dest_len; ++i)
      dst[i] = (group_id + i) % mcast_client_get_group_count(c->c);
  }

  if (verbose)
    printf("Client sending message '%.16s' to %d group(s)\n", c->v.value, c->v.dest_len);

  mcast_client_submit(c->c, (char*)&c->v, sizeof(struct client_value) - VSIZE + c->v.size, dst, c->v.dest_len);
}

// Returns t2 - t1 in microseconds.
static long
timeval_diff(struct timeval* t1, struct timeval* t2)
{
  long us;
  us = (t2->tv_sec - t1->tv_sec) * 1e6;
  if (us < 0)
    return 0;
  us += (t2->tv_usec - t1->tv_usec);
  return us;
}

static void
on_send_requests(int fd, short event, void* arg)
{
  struct client* c = arg;

  struct timeval now;
  gettimeofday(&now, NULL);
  int run_time_usec = timeval_diff(&c->send_start, &now);
  int send_target = reqs_per_sec * ((float)run_time_usec / 1000000.0);
  int to_send = send_target - c->total_sent;
  while (to_send--) {
    c->total_sent++;
    client_submit_value(c);
  }

  evtimer_add(c->send_ev, &c->send_interval);
}

static void
on_connect(struct mcast_client* mc, void* arg)
{
  // int            i;
  struct client* c = arg;

  stats_init();
  random_string(c->v.value, msg_size);
  c->v.size = msg_size;
  usleep(rand() % 1000);
  gettimeofday(&c->send_start, NULL);
  c->send_ev = evtimer_new(c->base, on_send_requests, c);
  evtimer_add(c->send_ev, &c->send_interval);
  // for (i = 0; i < outstanding; ++i)
  //   client_submit_value(c);
}

static void
on_read(struct mcast_client* mc, char* resp, int resp_len, void* ctx)
{
  // struct client*       c = ctx;
  struct client_value* v = (struct client_value*)resp;
  struct timespec      now;
  long                 latency;

  if (resp_len) {
    clock_gettime(CLOCK_MONOTONIC, &now);
    latency = (now.tv_sec - v->t.tv_sec) * 1e6 + (now.tv_nsec - v->t.tv_nsec) * 1e-3;
    stats_add(latency, v->dest_len);

    if (verbose)
      printf("Client %" PRIu64 " received %zu bytes - value %.16s\n", mcast_client_get_uid(mc), v->size, v->value);

    // client_submit_value(c);
  }
}

static struct client*
make_client(struct event_base* base)
{
  struct client* c;

  c = malloc(sizeof(struct client));
  c->base = base;
  c->c = mcast_client_connect(group_id, node_id, config, base, on_connect, on_read, c);
  c->send_interval = (struct timeval){ .tv_sec = 0, .tv_usec = 1000 }; // every 1ms
  if (c->c == NULL) {
    printf("Error connecting, exiting...\n");
    free(c);
    return NULL;
  }
  printf("Client with %d reqs/sec\n", reqs_per_sec);
  return c;
}

static void
client_free(struct client* c)
{
  mcast_client_free(c->c);
  event_free(c->send_ev);
  free(c);
}

static void
usage(const char* prog)
{
  printf("Usage: %s -n <node-id> -g <group_id> -c <path/to/mcast.conf> [-a] <percentage> [-r] <req-per-sec> [-s] "
         "<msg-size> [-h] [-v]\n",
         prog);
  printf("  %-30s%s\n", "-n, --node-id", "the node id");
  printf("  %-30s%s\n", "-g, --group-id", "the group id");
  printf("  %-30s%s\n", "-c, --config-file", "path to multicast configuration file");
  printf("  %-30s%s\n", "-a, --multigroup-percentage", "porcentage of multigroup commands (defaults to 0)");
  printf("  %-30s%s\n", "-d, --destinations", "number of destination groups for multigroup messages (defaults to 2)");
  printf("  %-30s%s%d%s\n", "-r, --requests", "target number of requests per second (defaults to ", reqs_per_sec,
         " bytes)");
  printf("  %-30s%s%d%s\n", "-s, --message-size", "size of the message in bytes (defaults to ", msg_size, " bytes)");
  printf("  %-30s%s\n", "-h, --help", "output this message and exit");
  printf("  %-30s%s\n", "-v, --verbose", "increase verbosity (print delivered messages)");
  exit(EXIT_FAILURE);
}

int
main(int argc, char* argv[])
{
  int                  opt = 0, idx = 0;
  struct client*       client;
  struct event_base*   base;
  struct event*        sig;
  static struct option options[] = { { "node-id", required_argument, 0, 'n' },
                                     { "group-id", required_argument, 0, 'g' },
                                     { "config-file", required_argument, 0, 'c' },
                                     { "multigroup-percentage", required_argument, 0, 'a' },
                                     { "destinations", required_argument, 0, 'd' },
                                     { "message-size", required_argument, 0, 's' },
                                     { "requests", no_argument, 0, 'r' },
                                     { "verbose", no_argument, 0, 'v' },
                                     { "help", no_argument, 0, 'h' },
                                     { 0, 0, 0, 0 } };

  while ((opt = getopt_long(argc, argv, "hvn:g:c:a:d:s:r:", options, &idx)) != -1) {
    switch (opt) {
      case 'n':
        node_id = atoi(optarg);
        break;
      case 'g':
        group_id = atoi(optarg);
        break;
      case 'a':
        global = atoi(optarg);
        break;
      case 'd':
        dest_len = atoi(optarg);
        break;
      case 's':
        msg_size = atoi(optarg);
        break;
      case 'r':
        reqs_per_sec = atoi(optarg);
        break;
      case 'c':
        config = optarg;
        break;
      case 'v':
        verbose = 1;
        break;
      default:
        usage(argv[0]);
    }
  }

  if (group_id == -1 || node_id == -1 || strlen(config) == 0)
    usage(argv[0]);

  srand(time(NULL));
  base = event_base_new();
  client = make_client(base);
  if (client) {
    sig = evsignal_new(base, SIGINT, handle_sigint, base);
    evsignal_add(sig, NULL);
    event_base_dispatch(base);
    client_free(client);
    event_free(sig);
  }

  event_base_free(base);
  return 0;
}
