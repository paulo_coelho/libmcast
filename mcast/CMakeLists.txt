INCLUDE_DIRECTORIES(${CMAKE_SOURCE_DIR}/mcast/include)

SET(SRCS mcast.c carray.c mcast_node.c mcast_persistence.c mcast_persistence_hash.c)

IF (LMDB_FOUND)
	LIST(APPEND SRCS mcast_persistence_lmdb.c)
ENDIF ()

ADD_LIBRARY(mcast STATIC ${SRCS})
TARGET_LINK_LIBRARIES(mcast ${LIBMCAST_LINKER_LIBS})

set_target_properties(mcast PROPERTIES
	INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib")

INSTALL(FILES include/mcast_types.h
              include/mcast.h 
  DESTINATION include)


IF (LMDB_FOUND)
	INCLUDE_DIRECTORIES(${LMDB_INCLUDE_DIRS})
	TARGET_LINK_LIBRARIES(mcast ${LMDB_LIBRARIES})
ENDIF ()
