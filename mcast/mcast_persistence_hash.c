/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "khash.h"
#include "mcast_persistence.h"

KHASH_MAP_INIT_INT64(record, mcast_message*)

struct hash_persist
{
  m_ts_t       trim_ts;
  kh_record_t* records;
};

static struct hash_persist*
hash_persist_new(int node_id)
{
  struct hash_persist* s = malloc(sizeof(struct hash_persist));
  if (s == NULL)
    return s;
  s->trim_ts = 0;
  s->records = kh_init(record);
  return s;
}

static int
hash_persist_open(void* handle)
{
  return 0;
}

static void
hash_persist_close(void* handle)
{
  struct hash_persist* s = handle;
  mcast_message*       msg;
  kh_foreach_value(s->records, msg, mcast_message_free(msg));
  kh_destroy_record(s->records);
  free(s);
}

static int
hash_persist_tx_begin(void* handle)
{
  return 0;
}

static int
hash_persist_tx_commit(void* handle)
{
  return 0;
}

static void
hash_persist_tx_abort(void* handle)
{
}

static int
hash_persist_get(void* handle, m_uid_t uid, mcast_message* out)
{
  khiter_t             k;
  struct hash_persist* s = handle;
  k = kh_get_record(s->records, uid);
  if (k == kh_end(s->records))
    return 0;
  if (out)
    mcast_message_copy(out, kh_value(s->records, k), 1);
  return 1;
}

static int
hash_persist_put(void* handle, mcast_message* msg)
{
  int                  rv;
  khiter_t             k;
  struct hash_persist* s = handle;
  mcast_message*       record = mcast_message_clone(msg, 1);
  k = kh_put_record(s->records, msg->uid, &rv);
  if (rv == -1) { // error
    mcast_message_free(record);
    return -1;
  }
  if (rv == 0) { // key is already present
    mcast_message_free(kh_value(s->records, k));
  }
  kh_value(s->records, k) = record;
  return 0;
}

static int
hash_persist_count(void* handle)
{
  struct hash_persist* s = handle;
  return kh_size(s->records);
}

static int
hash_persist_trim(void* handle, m_ts_t ts)
{
  khiter_t             k;
  struct hash_persist* s = handle;

  if (s->trim_ts > ts)
    return 1;

  for (k = kh_begin(s->records); k != kh_end(s->records); ++k) {
    if (kh_exist(s->records, k)) {
      mcast_message* msg = kh_value(s->records, k);
      if (msg->timestamp > 0 && msg->timestamp <= ts) {
        mcast_message_free(msg);
        kh_del_record(s->records, k);
      }
    }
  }
  s->trim_ts = ts;
  return 0;
}

static m_ts_t
hash_persist_get_trim(void* handle)
{
  struct hash_persist* s = handle;
  return s->trim_ts;
}

void
mcast_persist_init_hash(struct mcast_persist* s, int node_id)
{
  s->handle = hash_persist_new(node_id);
  s->api.open = hash_persist_open;
  s->api.close = hash_persist_close;
  s->api.tx_begin = hash_persist_tx_begin;
  s->api.tx_commit = hash_persist_tx_commit;
  s->api.tx_abort = hash_persist_tx_abort;
  s->api.get = hash_persist_get;
  s->api.put = hash_persist_put;
  s->api.size = hash_persist_count;
  s->api.trim = hash_persist_trim;
  s->api.get_trim = hash_persist_get_trim;
}
