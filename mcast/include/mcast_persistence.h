/*
 * Copyright (c) 2016, University of Lugano
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the copyright holders nor the names of it
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _MCAST_PERSISTENCE_H_
#define _MCAST_PERSISTENCE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "mcast.h"

struct mcast_persist
{
  void* handle;

  struct
  {
    int (*open)(void* handle);

    void (*close)(void* handle);

    int (*tx_begin)(void* handle);

    int (*tx_commit)(void* handle);

    void (*tx_abort)(void* handle);

    int (*get)(void* handle, m_uid_t uid, mcast_message* out);

    int (*put)(void* handle, mcast_message* acc);

    int (*size)(void* handle);
    
    int (*trim) (void* handle, m_ts_t ts);
    
    m_ts_t (*get_trim) (void* handle);
  } api;
};

void mcast_persist_init(struct mcast_persist* store, int node_id, const char* suffix);

int mcast_persist_open(struct mcast_persist* store);

void mcast_persist_close(struct mcast_persist* store);

int mcast_persist_tx_begin(struct mcast_persist* store);

int mcast_persist_tx_commit(struct mcast_persist* store);

void mcast_persist_tx_abort(struct mcast_persist* store);

int mcast_persist_get_record(struct mcast_persist* store, m_uid_t uid, mcast_message* out);

int mcast_persist_put_record(struct mcast_persist* store, mcast_message* msg);

int mcast_persist_get_record_count(struct mcast_persist* store);

int mcast_persist_trim(struct mcast_persist* store, m_ts_t ts);

m_ts_t mcast_persist_get_trim(struct mcast_persist* store);

char* mcast_message_to_buffer(mcast_message* msg, int* buffer_size);

void mcast_message_from_buffer(char* buffer, mcast_message* out);

void mcast_persist_init_lmdb(struct mcast_persist* s, int node_id, const char* suffix);

void mcast_persist_init_hash(struct mcast_persist* s, int node_id);

#ifdef __cplusplus
}
#endif

#endif
