# - Find LIBPAXOS
# Find the native LibPaxos includes and library
#
# LIBPAXOS_INCLUDES - where to find evpaxos.h
# LIBPAXOS_LIBRARIES - List of libraries when using LIBPAXOS.
# LIBPAXOS_FOUND - True if LibPaxos found.

set(LIBPAXOS_ROOT "" CACHE STRING "LIBPAXOS root directory")

find_path(LIBPAXOS_INCLUDE_DIR evpaxos.h 
    HINTS "${LIBPAXOS_ROOT}/include")

find_library(LIBPAXOS_LIBRARY
   NAMES evpaxos
   HINTS "${LIBPAXOS_ROOT}/lib")

set(LIBPAXOS_LIBRARIES ${LIBPAXOS_LIBRARY})
set(LIBPAXOS_INCLUDE_DIRS ${LIBPAXOS_INCLUDE_DIR})

include(FindPackageHandleStandardArgs)

# handle the QUIETLY and REQUIRED arguments and set LIBPAXOS_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LIBPAXOS DEFAULT_MSG
                                  LIBPAXOS_LIBRARY LIBPAXOS_INCLUDE_DIR)

mark_as_advanced(LIBPAXOS_INCLUDE_DIR LIBPAXOS_LIBRARY)
